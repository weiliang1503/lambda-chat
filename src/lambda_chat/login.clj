(ns lambda-chat.login
  (:require
   [clojure.java.io :as io]
   [clojure.data.json :as json])
  (:import
   [java.awt BorderLayout]
   [java.awt.event ActionListener]
   [javax.swing JButton JFrame JLabel JOptionPane JPanel JTextField JPasswordField Box JCheckBox ImageIcon JComboBox]
   [dev.dirs BaseDirectories]))

(defn login-window [callback]
  (let [num-field (JTextField. "" 20)
        pas-field (doto (JPasswordField. "" 20) (.setEchoChar \*))
        greet-button (JButton. "登錄")
        box (. Box createVerticalBox)
        frame (JFrame.)
        checkbox (JCheckBox. "記住我")
        protocol-selector (doto (JComboBox.)
                            (.addItem "手機")
                            (.addItem "平板")
                            (.addItem "手錶"))
        
        config-dir (str (. (. BaseDirectories get) configDir) "/lambda-chat");;獲得XDG風格的路徑
        config-file (str config-dir "/login.json")
        config (if (.exists (io/file config-file)) (json/read-str (slurp config-file)) {})

        listener (proxy [ActionListener] []
                   (actionPerformed [e]

                     (let [num-str (.getText num-field)
                           pas-str (String/valueOf (.getPassword pas-field))]
                       (if (and (not (= num-str "")) (not (= pas-str "")) (= Long (class (read-string num-str))))
                         (do ;;(.setVisible frame false)\
                           (if (.isSelected checkbox)
                             (do (def num-out num-str)
                                 (def pas-out pas-str))
                             (do (def num-out "")
                                 (def pas-out "")))
                           ;;(println config-file)
                           (when (not (.exists (io/file config-dir)))
                             (.mkdir (io/file config-dir)))

                           ;;生成新的配置
                           (def new-config {"number" num-out
                                            "password" pas-out
                                            "protocol" (.getSelectedIndex protocol-selector)
                                            "autologin" (.isSelected checkbox)})
                           ;;若配置有變化，則更新配置
                           (when (not (= config new-config))
                             (spit config-file (json/write-str new-config)))

                           (.dispose frame)
                           (callback (read-string num-str)  pas-str (.getSelectedIndex protocol-selector)))

                         (JOptionPane/showMessageDialog nil "請正確輸入！")))))]

    (when (.exists (io/file config-file))
      ;;(def config (json/read-str (slurp config-file)))
      (.setText num-field (config "number"))
      (.setText pas-field (config "password"))
      (.setSelectedIndex protocol-selector (config "protocol"))
      (.setSelected checkbox (config "autologin")))

    (doto box
      (.add (doto (JPanel.)
              (.add (doto (JLabel.)
                      (.setIcon (ImageIcon. (io/resource "lambda-chat.png")))))))
      (.add (Box/createVerticalGlue))
      (.add (doto (JPanel.)
              (.add (JLabel. "帳號"))
              (.add num-field)))
      (.add (Box/createVerticalGlue))
      (.add (doto (JPanel.)
              (.add (JLabel. "密碼"))
              (.add pas-field)))
      (.add (Box/createVerticalGlue))
      (.add (doto (JPanel.)
              (.add protocol-selector)
              (.add checkbox)))
      (.add (Box/createVerticalGlue)))

    (doto frame
      (.add box BorderLayout/CENTER)
      (.add greet-button BorderLayout/SOUTH)
      (.setTitle "請登錄")
      (.pack)
      (.setLocationRelativeTo nil)
      (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE)
      (.setVisible true))

    (.addActionListener greet-button listener)))

