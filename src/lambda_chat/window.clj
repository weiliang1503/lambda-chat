(ns lambda-chat.window
  (:import
   [java.awt BorderLayout]
   [java.awt.event ActionListener]
   [javax.swing JButton JFrame JLabel JOptionPane JPanel JTextField JPasswordField Box JCheckBox ImageIcon JComboBox JTabbedPane]))

(defn make-main-window []
  (let [frame (JFrame.)
        pane (JTabbedPane. JTabbedPane/LEFT)]
    (doto frame
      (.add pane BorderLayout/CENTER)
      (.setTitle "Lambda-chat")
      (.pack)
      (.setVisible true)
      (.setSize 800 600))
    {:frame frame :pane pane}))
