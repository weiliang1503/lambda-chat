(ns lambda-chat.qq
  (:require [lambda-chat.window :as window])
  (:import [net.mamoe.mirai Bot BotFactory]
           [net.mamoe.mirai.event.events MessageEvent FriendMessageEvent GroupMessageEvent]
           [net.mamoe.mirai.message.data MessageChainBuilder QuoteReply]
           [net.mamoe.mirai.utils BotConfiguration BotConfiguration$MiraiProtocol]
           [net.mamoe.mirai.contact Friend Group]

           [java.awt BorderLayout]
           [java.awt.event ActionListener]
           [javax.swing JButton JFrame JLabel JOptionPane JPanel JTextField JPasswordField Box JCheckBox ImageIcon JComboBox JTabbedPane]))

(defn after-login [bot]

  (def chat-list (atom {}))

  (def widgets (window/make-main-window))
  (def frame (widgets :frame))
  (def pane (widgets :pane))
  
  (.subscribeAlways (.getEventChannel bot)
                    MessageEvent
                    (reify java.util.function.Consumer
                      (accept [this event]

                        (when (not (contains? @chat-list (.getSubject event)))
                          (let [new-box (. Box createVerticalBox)]
                            (swap! chat-list #(assoc % (.getSubject event) new-box))
                            (.addTab pane (.toString (.getSubject event)) new-box)))

                        (doto (@chat-list (.getSubject event))
                          (.add (JLabel. (.toString (.getMessage event)))))))))

(defn login-account [num pas protocol]
  (let [my-protocol (cond (= protocol 0) BotConfiguration$MiraiProtocol/ANDROID_PHONE
                          (= protocol 1) BotConfiguration$MiraiProtocol/ANDROID_PAD
                          (= protocol 2) BotConfiguration$MiraiProtocol/ANDROID_WATCH)
        bot (. (. BotFactory INSTANCE) newBot num pas (doto (BotConfiguration.)
                                                        (.setProtocol my-protocol)))]
    (.login bot)
    (after-login bot)))
  
