(ns lambda-chat.core
  (:require [lambda-chat.login :as app]
            [lambda-chat.qq  :as qq])
  (:gen-class))

(defn -main [& args]
  ;;喚出登錄窗口
  (app/login-window
   ;;回調，使用mirai登錄
   (fn [num pas protocol]
     (qq/login-account num pas protocol)
     )))
