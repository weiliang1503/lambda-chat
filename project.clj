(defproject lambda-chat "0.1.0-SNAPSHOT"
  :description "A chatting client written clojure based on Mirai"
  :url "http://example.com/FIXME"
  :license {:name "GNU GPL3.0 or later"
            :url "https://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [net.mamoe/mirai-core-jvm "2.11.0-M2.2"]
                 [org.clojure/java.jdbc "0.7.12"]
                 [org.xerial/sqlite-jdbc "3.36.0.3"]
                 [org.clojure/data.json "2.4.0"]
                 [dev.dirs/directories "26"]]
  :main lambda-chat.core
  :aot :all;;[lambda-chat.core]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
